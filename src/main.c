#include <signal.h>
#include <stdio.h>

#include "shell.h"
#include "tio.h"

int main()
{
    setbuf(stdout, NULL);
    setbuf(stderr, NULL);

    backup_tio();
    set_tio();

    signal(SIGINT, sigint_handler())

    int return_value = input_loop();

    restore_tio();

    return return_value;
}