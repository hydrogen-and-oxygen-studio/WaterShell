# WaterShell
Our own shell.

# How to use it?
1. Enter the WaterShell folder`cd WaterShell`
2. Make the file`make`
3. Enter the `bin` folder`cd bin`
4. Enter`./WaterShell`

# LICENSE
GPL-V3.0, User License Agreement


###### WaterShell Copyright (C) 2019 Hydrogen and Oxygen Studio
